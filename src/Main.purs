module Main where

import Prelude -- must be explicitly imported
import Effect (Effect)
import Effect.Console (log)
import Data.Array (length, uncons, slice, (:), last)
import Data.Array.Partial (head)
import Data.Functor
-- import Data.Generic
import Data.Int
import Data.Maybe
import Data.Traversable
import Data.Tuple
import Graphics.Canvas (closePath, lineTo, moveTo, fillPath,
                        getCanvasElementById, Context2D, Rectangle, clearRect)
import Partial.Unsafe (unsafePartial, unsafeCrashWith)
import Signal (Signal, runSignal, foldp, sampleOn, map4)
import Test.QuickCheck.Gen -- for randomness

type Point = Tuple Int Int

type Snake = Array Point

type Model = { xd :: Int, yd :: Int, size :: Int, mouse:: Point
             , snake :: Snake, dir :: Point, alive :: Boolean
             , prev :: Maybe Point}

input :: Effect (Signal Point)
input = unsafeCrashWith "jn39s8&"

render :: Partial => Model -> Effect Unit
render m = unsafeCrashWith "dkn2t02"

renderStep :: Partial => Model -> Effect Unit
renderStep = unsafeCrashWith "mn28621"
 
init :: Effect Model
init = unsafeCrashWith "tm36kc8"
       -- evalGenD init'
       
--need 2nd argument to be Eff for foldp
step :: Partial => Point -> Model -> Model 
step = unsafeCrashWith "mn3,b000"

main :: Effect Unit
main = --onDOMContentLoaded 
    void $ unsafePartial do
      --draw the board
      gameStart <- init
      render gameStart
      -- create the signals
      dirSignal <- input
      -- need to be in effect monad in order to get a keyboard signal
      let game = foldp step gameStart dirSignal --- was 
      runSignal (map renderStep game)
