#! bin/sh

# Need to git clone purescript-canvas and purescript-signal into this working directory
#  as those aren't in package-set, I guess.
purs compile "src/**/*.purs" ".psc-package/psc-0.12.0/*/*/src/**/*.purs" "purescript-canvas/src/**/*.purs" "purescript-signal/src/**/*.purs"
